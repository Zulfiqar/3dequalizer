#
#
# 3DE4.script.name:	Save Incremental
#
# 3DE4.script.version:	v0.3
#
# 3DE4.script.gui:	Main Window::3DE4
#
# 3DE4.script.comment:	Saves current file to project directory adding a versioned number.
# 3DE4.script.comment:  Expects to find _v###.3de in file name, else will be added. If the
# 3DE4.script.comment:  file hasn't been saved yet, open a file save dialog box.
#
# 2015, Hans Hoogenboom
# 


import os.path
import re

# get current file and path
currentFilePath = tde4.getProjectPath()
if currentFilePath != None:
    currentFile     = os.path.basename( currentFilePath )
    currentPath     = os.path.dirname( currentFilePath )

    # find version pattern
    pattern = "_v[0-9]*"
    compiled = re.compile( pattern )
    verObject = re.search( compiled, currentFile )
    if verObject:
        intPart = int( verObject.group()[2:] )
        intPart = intPart + 1
        newFile = currentFile[ : verObject.start() + 2 ] + str( intPart ) + currentFile[ verObject.end() : ]
    else:
        intPart = "_v001"
        (name, ext) = os.path.splitext( currentFile )
        newFile = name + intPart + ext

    # save file
    newPath = currentPath + '/' + newFile
    existingFile = os.path.exists( newPath )

    action = 0
    if existingFile:
        # ok == 0, cancel == 1
        action = tde4.postQuestionRequester( "Warning", "File already exists, overwrite?", "OK", "Cancel" )

        # write if OK is clicked
        if action == 0:
            try:
                os.remove( newPath )
            except:
                tde4.postQuestionRequester( "Failure", "Could not remove existing file %s" % s, "OK" )
                action = 1

    if action == 0:
        result = tde4.saveProject( newPath )
        if result == 0:
            tde4.postQuestionRequester( "Failure", "Could not write file to disk.", "OK" )  
else:
    currentPath = tde4.postFileRequester( "Save Project As...", "*.3de" )
    result = tde4.saveProject( currentPath )
