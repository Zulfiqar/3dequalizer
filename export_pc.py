#
#
# 3DE4.script.name:	Export Pointcloud...
#
# 3DE4.script.version:	v0.1
#
# 3DE4.script.gui:	Main Window::3DE4::File::Export
# 3DE4.script.gui:      Object Browser::Context Menu Point
#
# 3DE4.script.comment:	Exports pointcloud as obj file.
# 3DE4.script.comment:  When called from the 3DE4 file menu, export all points in the
# 3DE4.script.comment:  current pointgroup, when called from Object browser only export
# 3DE4.script.comment:  selected points. Only tracked 3D points are exported.
#
# 2015 Hans Hoogenboom
#


# TODO: set callback on file extension
# TODO: do something with scene transformation
# TODO: implement export from main menu


import os.path
import datetime


def userInterface():
    req	= tde4.createCustomRequester()
    tde4.addFileWidget( req, "filebrowser", "Save to...", "*.obj")
    tde4.addOptionMenuWidget( req, "filetypes", "File type", "obj", "ply" )
    ret	= tde4.postCustomRequester(req,"Pointcloud...",500,0,"Ok","Cancel")
    if ret == 1:
        filePath = tde4.getWidgetValue( req, "filebrowser" )
        fileType = tde4.getWidgetValue( req, "filetypes" )
        return( filePath, fileType )
    else:
        return None


def writeWavefrontOBJ( vectors, fp ):
    now = datetime.datetime.now()
    fp.write( "# 3Dequalizer: %s\n" % tde4.getProjectPath() )
    fp.write( "# Time: %s\n" % now )
    fp.write( "# Number of verts: %s\n" % len( vectors ) )
    fp.write( "\n" )
    fp.write( "# vertices\n" )
    for vec in vectors:
        fp.write( "v %f %f %f 1.0\n" % ( vec[0], vec[1], vec[2] ) )


def writePLY( vectors, fp ):
    now = datetime.datetime.now()
    fp.write( "ply\n" )
    fp.write( "format ascii 1.0\n" )
    fp.write( "comment 3Dequalizer: %s\n" % tde4.getProjectPath() )
    fp.write( "comment Time: %s\n" % now )
    fp.write( "element vertex %i\n" % len( vectors ) )
    fp.write( "property float x\n" )
    fp.write( "property float y\n" )
    fp.write( "property float z\n" )
    #fp.write( "property uchar red\n" )
    #fp.write( "property uchar green\n" )
    #fp.write( "property uchar blue\n" )
    #fp.write( "property list uchar int\n" )
    fp.write( "end_header\n" )
    for vec in vectors:
        fp.write( "%f %f %f\n" % ( vec[0], vec[1], vec[2] ) )

# Main
context = tde4.getContextMenuObject()

scenePos = tde4.getScenePosition3D()
sceneScl = tde4.getSceneRotation3D()
sceneRot = tde4.getSceneScale3D()

# called from object browser
if context:
    pointGroup = tde4.getContextMenuParentObject()
    if pointGroup != None:
        pointList = tde4.getPointList( pointGroup, 1 )
# called from menu
else:
    pointGroup = tde4.getCurrentPGroup()    
    if pointGroup != None:
        pointList = tde4.getPointList( pointGroup, 0 )
    
if pointGroup:
    if len( pointList ) > 1:
        exportList = []
        for point in pointList:
            if tde4.isPointCalculated3D( pointGroup, point ):
                exportList.append( point )

        if len( exportList ) > 0:
            vec = []
            #col = []
            for point in exportList:
                vec.append( tde4.getPointCalcPosition3D( pointGroup, point ) )
                #col.append( tde4.getPointColor3D( pointGroup, point ) )
                print tde4.getPointRGBWeights( pointGroup, point )
            # show user interface
            (filePath, fileType) = userInterface()
            fileType = fileType - 1
            # check if file already exists and if we want to overwrite
            safe = True
            if os.path.isfile( filePath ):
                if tde4.postQuestionRequester( "Warning", "File exists. Overwrite?", "OK", "Cancel" ) == 1:
                    safe = False
            # file does not exist, append file extension
            else:
                ext = ['obj', 'ply']
                filePath = filePath + "." + ext[ fileType ]
            # write data
            if safe:
                if fileType == 0:
                    with open( filePath, 'w' ) as fp:
                        writeWavefrontOBJ( vec, fp )
                if fileType == 1:
                    with open( filePath, 'w' ) as fp:
                        writePLY( vec, fp )
        else:
            tde4.postQuestionRequester( "Pointcloud...","There are no 3D points calculated.","Ok" )

    else:
        tde4.postQuestionRequester( "Pointcloud...","There are no points selected.","Ok" )
else:
    tde4.postQuestionRequester( "Pointcloud...", "Error, no point group", "OK" )
