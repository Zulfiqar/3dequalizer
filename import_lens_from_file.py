#
#
# 3DE4.script.name:     Import Lens From File...
#
# 3DE4.script.version:  v0.3
#
# 3DE4.script.gui:      Main Window::Presets
#
# 3DE4.script.comment:  Creates a list of lenses in a directory and sets a lens
# 3DE4.script.comment:  on a selected camera. Does not support animation curves.
#
# 3DE4.script.hide: 	false
# 3DE4.script.startup: 	false
#
# v0.0.1 17th February 2015 by Hans Hoogenboom
# v0.3   24th March
# v0.4   12th July, reversed FBwidth/height
#
#

import os
import fnmatch


class parmLD( object ):
    def __init__( self ):
        self.name = None
        self.value = None

    def allset( self ):
        if self.name == None or self.value == None:
            return False
        else:
            return True

    def reset( self ):
        self.name = None
        self.value = None
 
     
# strip the file of the path
def checkDir( path ):
    if os.path.isdir( path ):
        return path
    else:
        return os.path.dirname( path )


# repopulate the lens list when the directory or
# the filter changes.
def _populateListCallback( req, widget, action ):
    tde4.removeAllListWidgetItems( req, "lenslist" )
    pattern = tde4.getWidgetValue( req, "filter" )
    
    # in case the user does select a file instead of just clicking
    # ok in the file widget, strip the file of the path
    targetDir = checkDir( tde4.getWidgetValue( req, "file_browser") )
    lensList = fnmatch.filter( os.listdir( targetDir ), pattern )
    
    lensListLength = len( lensList )
    for lens in lensList:
        tde4.insertListWidgetItem( req, "lenslist", lens, 0 )


def readCurve():
    pass


def readLensFile( lenspath ):
    newLens = tde4.createLens()

    with open( lenspath, 'r' ) as fp:
        # first line holds lens name
        line = fp.readline()
        name = line.strip()
        tde4.setLensName( newLens, name )

        # second lines holds the focal length, film aspect,
        # lens center offsets, film back and pixel aspect.
        line = fp.readline()
        parm = [ float( i ) for i in line.split() ]
        tde4.setLensFBackWidth(  newLens, parm[0] )
        tde4.setLensFBackHeight( newLens, parm[1] )
        tde4.setLensFocalLength( newLens, parm[2] )
        tde4.setLensFilmAspect(  newLens, parm[3] )
        tde4.setLensLensCenterX( newLens, parm[4] )
        tde4.setLensLensCenterY( newLens, parm[5] )
        tde4.setLensPixelAspect( newLens, parm[6] )

        # dynamic distortion or not
        line = fp.readline()
        dist_type = line.strip()
        if dist_type == "DISTORTION_STATIC":
            tde4.setLensDynamicDistortionFlag( newLens, 0 )
        else:
            tde4.setLensDynamicDistortionFlag( newLens, 1 )
        tde4.setLensDynamicDistortionMode( newLens, dist_type )

        # lens distortion model
        line = fp.readline()
        dist_model = line.strip()
        tde4.setLensLDModel( newLens, dist_model )

        # set distoration parameters
        lensParm = parmLD()
        ld_line = "0"
        while ld_line != "<end_of_file>" and ld_line != "None":
            # a float value
            if '.' in ld_line:
                if ' ' in ld_line:
                    # probably a curve
                    pass
                else:
                    lensParm.value = float( ld_line )
            # toggle?
            elif ld_line == "0":
                pass
            # string
            elif len( ld_line ) > 1:
                lensParm.name = ld_line
            else:
                print "Unrecognized value %s" % ld_line

            if lensParm.allset():
                tde4.setLensLDAdjustableParameter( newLens, str(lensParm.name), parm[2], 100.0, lensParm.value )
                lensParm.reset()
            
            ld_line = fp.readline()
            ld_line = str( ld_line.strip() )

        return newLens
    

# default start up directory
try:
    defaultDir = os.environ['3DE4_LENS_DIR']
except:
    defaultDir = tde4.getProjectPath()
    if not defaultDir:
        defaultDir = tde4.get3DEInstallPath()
        if not defaultDir:
            defaultDir = os.getcwd()

cam = tde4.getCameraList( 1 )
if not cam:
    camWarning = tde4.postQuestionRequester( "Warning", "You must select a camera", "OK" )
elif cam:
    currentCam = cam[0]
    
    # create UI
    req = tde4.createCustomRequester()
    # widgets    
    tde4.addFileWidget( req, "file_browser", "Directory...", "*", defaultDir )
    tde4.addTextFieldWidget( req, "filter", "Lens Filter Pattern", "*.txt" )
    tde4.addListWidget( req, "lenslist", "Lenses", 0, 100 )
    tde4.addToggleWidget( req, "delete_lens", "Delete Current Lens", 0 )
    # callback functions to redraw the list
    tde4.setWidgetCallbackFunction( req, "file_browser", "_populateListCallback" )
    tde4.setWidgetCallbackFunction( req, "filter", "_populateListCallback" )
    # show UI
    ret = tde4.postCustomRequester( req, "Import Lens From File...", 600, 0, "Add", "Cancel" )
    
    lenspath = None
    targetDir = checkDir( tde4.getWidgetValue( req, "file_browser" ) )
    if ret == 1:
        numitems = tde4.getListWidgetNoItems( req, "lenslist" )
        itemlist = range(0, numitems )
        for items in itemlist:
            selflag = tde4.getListWidgetItemSelectionFlag( req, "lenslist", items )
            if selflag == 1:
                itemname = tde4.getListWidgetItemLabel( req, "lenslist", items )
                lenspath = os.path.join(targetDir, itemname)

        delLens = tde4.getWidgetValue( req, "delete_lens" )
        # if lenspath == None we get an error
        if lenspath:
            newLens = readLensFile( lenspath )

            if newLens:
                lens = tde4.getCameraLens( currentCam )

                if delLens and lens:
                    tde4.deleteLens ( lens )
                    tde4.setCameraLens( currentCam, newLens )
                else:
                    tde4.setCameraLens( currentCam, newLens )
            else:
                fileWarning = tde4.postQuestionRequester( "Warning", "Could not read lens preset", "OK" )
        else:
            lensWarning = tde4.postQuestionRequester( "Warning", "No lens preset selected", "OK" )
 
    tde4.deleteCustomRequester( req )
