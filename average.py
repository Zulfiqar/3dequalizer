#
#
# 3DE4.script.name:     Insert average point
#
# 3DE4.script.version:  v0.1
#
# 3DE4.script.gui.button: Manual Tracking Controls::Average Point, align-bottom-left, 80, 20
# 3DE4.script.gui         Main Window::Utilities
#
# 3DE4.script.comment:  Add the average of two selected tracked points.
#
# 3DE4.script.hide: 	false
# 3DE4.script.startup: 	false
#
# v0.1 12th July 2015 by Hans Hoogenboom
#
#

pgroup_id = tde4.getCurrentPGroup()
camera_id = tde4.getCurrentCamera()
playback  = tde4.getCameraPlaybackRange(camera_id)

points = tde4.getPointList(pgroup_id, 1)

if len(points) == 2:
    newPoint = tde4.createPoint(pgroup_id)
    tde4.setPointName(pgroup_id, newPoint, "AVG")

    for frame in range(playback[0], playback[-1]+1):
        valid = False
        point_pos = [0,0]
        for i in [0,1]:
            valid = tde4.isPointPos2DValid(pgroup_id, points[i], camera_id, frame)
            if valid:
                point_pos = [sum(x) for x in zip(point_pos, tde4.getPointPosition2D(pgroup_id, points[i], camera_id, frame))]
            else:
                break
        if valid:
            point_pos = [x * 0.5 for x in point_pos]
            tde4.setPointPosition2D(pgroup_id, newPoint, camera_id, frame, point_pos)

else:
    tde4.postQuestionRequester("Error","Select two points", "OK")


